//Tahle verze byla odevzdana 18:21:57 - 19.05.2016 (timestamp - 1463674945)

package robot;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by jiri on 5/17/16.
 */
public class Robot {


    public static void main(String[] args) {
        /*TODO: Zjisti ktere parametry mas zpracovat
        * TODO:
        * TODO:      Bude to 1 parametr v pripade prijmani fotografie okoli
        * TODO:      Pripadne 2 parametry pokud budeme chtit uploadnout novy firmware
        * */
        System.out.println("Initiating client...\n");
        if (args.length == 1){  //Jeden parametr => Prijem fotografie
            InetAddress serverAddr;
            //start
            try {
                serverAddr = InetAddress.getByName(args[0]);
            } catch (UnknownHostException e) {
                throw new IllegalArgumentException("Bad desination address");
            }

            //Tady poslu INIT a vytvorim photo
            DatagramSocket s = null;
            try {
                s = new DatagramSocket();
            } catch (SocketException e) {
                e.printStackTrace();
            }
            Connection c = new Connection(s,serverAddr);
            c.start();


        } else if (args.length == 2){ //Dva parametry => Upload firmwaru
            InetAddress serverAddr;
            //start
            try {
                serverAddr = InetAddress.getByName(args[0]);
            } catch (UnknownHostException e) {
                throw new IllegalArgumentException("Bad desination address");
            }
            try {
                FileReader firmwareFile = new FileReader(args[1]);
            } catch (FileNotFoundException e) {
                //e.printStackTrace();
                throw new IllegalArgumentException("File doesn't exist");
            }


        } else {    //nejaka volovina tady vyhodim nejakou exception
            throw new IllegalArgumentException("Bad argument/s");
        }
    }



}
class Init{}
class FirmwareUploader{}

class Connection extends Thread{
    private final DatagramSocket socket;
    private FotoLoader fotoLoader;
    private final InetAddress addr;
    private Byte[] connID;

    public Connection(DatagramSocket socket, InetAddress address) {
        this.socket = socket;
        try {
            this.socket.setSoTimeout(100);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        this.addr = address;
    }
    private void initDataComm(Byte[] connectionID){
        this.fotoLoader = new FotoLoader(connectionID);
        this.connID = connectionID;
    }

    private boolean initComm(){
        byte[] dataToSend = FotoLoader.constructInitialPacket();

        try {
            socket.send(new DatagramPacket(dataToSend,dataToSend.length,addr,4000));
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] data = new byte[Packet.HEADER_SIZE+1];
        DatagramPacket packetToRecieve = new DatagramPacket(data, data.length);
        try {
            socket.receive(packetToRecieve);
        } catch (SocketTimeoutException e){
            System.out.println("Init timeout");
            return initComm();
        }catch (IOException e) {
            System.out.println("init IO exp");
            return false;
        }
        byte[] recievedID = packetToRecieve.getData();
        Byte[] id = new Byte[4];
        id[0] = recievedID[0];
        id[1] = recievedID[1];
        id[2] = recievedID[2];
        id[3] = recievedID[3];

        int result = 0;
        int shift = 24;

        for (int i = 0; i < id.length; i++) {
            int part = (int)id[i] & 0xFF;

            result = result ^ (part << shift);
            shift -= 8;
        }
        System.out.println("recv ID: " + id[0] + " " + id[1] + " " + id[2] + " " + id[3]);
        try {
            initDataComm(id);
        } catch (IllegalArgumentException e){
            System.out.println("ID paket se ztratil");
            return initComm();


            /*byte[] toSend = new byte[9];
            if (fotoLoader != null){
                toSend = FotoLoader.constructErrorPacket(fotoLoader.baseCID.getCurrentConnectionID());
            } else {
                toSend[8] = Symptom.RST;
            }

            System.out.println("SENDRST: ");

            for (int i = 0; i < toSend.length; i++) {
                System.out.print(toSend[i] + " ");
            }

            try {
                socket.send(new DatagramPacket(toSend,toSend.length,addr,4000));
            } catch (IOException ee) {
                e.printStackTrace();
            }
            socket.close();
            return false;*/
        }
        return true;
    }
    private boolean dataComm(){
        byte[] data = new byte[264];
        DatagramPacket packetToRecieve = new DatagramPacket(data, data.length);
        try {
            socket.receive(packetToRecieve);
        }catch (SocketTimeoutException se){
            System.out.println("Comm timeout");
            dataComm();
            return true;
        }
        catch (IOException e) {
            System.out.println("Comm IO exp");
            //dataComm();
            return false;
        }
        if (data[8] == Symptom.FIN){
            System.out.println("\033[41;1m"+"Recieved last packet"+"\033[0;0m");
            System.out.println("Calling Mr. Proper");
            byte[] ack = {data[4],data[5]};
            Byte[] id = fotoLoader.baseCID.getCurrentConnectionID();
            byte[] dataToSend = FotoLoader.constructFinalPacket(id,ack);
            System.out.println("SENDFIN: ");
            for (int i = 0; i < dataToSend.length; i++) {
                System.out.print(dataToSend[i] + " ");
            }

            try {
                socket.send(new DatagramPacket(dataToSend,dataToSend.length,addr,4000));
            } catch (IOException e) {
                e.printStackTrace();
            }

            fotoLoader.save();

            for (int i = 0; i < 10; i++) {
                try {
                    socket.receive(packetToRecieve);
                }catch (SocketTimeoutException se){
                    System.out.println("Backup fin didnt came");
                    if (i < 9){
                        continue;
                    } else break;
                } catch (IOException er){
                    System.out.println("IO exp");
                }
                System.out.println("SENDFIN: ");
                for (int j = 0; j < dataToSend.length; j++) {
                    System.out.print(dataToSend[j] + " ");
                }

                try {
                    socket.send(new DatagramPacket(dataToSend,dataToSend.length,addr,4000));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            socket.close();


            return false;

        }
        if (data[8] == Symptom.RST){
            System.out.println("RST recieved");
            fotoLoader.save();
            socket.close();
            return false;
        }

        byte[] rec = {packetToRecieve.getData()[4],packetToRecieve.getData()[5]};
        System.out.print(" | recieved(" + SequentialNumber.getSequentialNumberDecimal(rec)+")");
        int ack = 0;
        try {
            ack = fotoLoader.recieve(packetToRecieve);
        } catch (IllegalArgumentException e){
            byte[] dataToSend = FotoLoader.constructErrorPacket(fotoLoader.baseCID.getCurrentConnectionID());
            System.out.println("SENDRST: ");

            for (int i = 0; i < dataToSend.length; i++) {
                System.out.print(dataToSend[i] + " ");
            }

            try {
                socket.send(new DatagramPacket(dataToSend,dataToSend.length,addr,4000));
            } catch (IOException ee) {
                e.printStackTrace();
            }

            socket.close();
            return false;
        }
        int ackToSend = (ack % 65535) + (255-ConfirmWindow.overFlowCNT);
        System.out.println(" | expecting: " +ackToSend);
        byte[] ackBigEnd = SequentialNumber.seqToBigEnd(ackToSend);
        byte[] dataToSend = new byte[Packet.HEADER_SIZE];
        dataToSend[0] = fotoLoader.baseCID.getCurrentConnectionID()[0];
        dataToSend[1] = fotoLoader.baseCID.getCurrentConnectionID()[1];
        dataToSend[2] = fotoLoader.baseCID.getCurrentConnectionID()[2];
        dataToSend[3] = fotoLoader.baseCID.getCurrentConnectionID()[3];

        //System.out.println("conn ID: " + dataToSend[0] + " " + dataToSend[1] + " " + dataToSend[2] + " " + dataToSend[3]);

        dataToSend[4] = 0x00;
        dataToSend[5] = 0x00;

        dataToSend[6] = ackBigEnd[0];//Ack
        dataToSend[7] = ackBigEnd[1];//Ack

        dataToSend[8] = 0x00;//symptom

        try {
            socket.send(new DatagramPacket(dataToSend,dataToSend.length,addr,4000));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void run() {
        synchronized (Connection.class){
            while (true){
                if (this.fotoLoader == null){
                    if (!initComm()){
                        break;
                    }
                } else {
                    if (!dataComm()){
                        break;
                    }
                }
            }
            this.currentThread().interrupt();

        }
    }
}

class FotoLoader{
    ConfirmWindow confirmWindow;
    ConnectionID baseCID;


    public FotoLoader(Byte[] connectionID) {
        baseCID = new ConnectionID();
        baseCID.saveRecievedID(connectionID);
        Photo p = new Photo();
        this.confirmWindow = new ConfirmWindow(p);
    }


    public int recieve(DatagramPacket datagramPacket){
        //Prijmany ack bude vzdy 0;
        //seq se meni
        int ack = confirmWindow.addPacket(new Packet(datagramPacket,baseCID,datagramPacket.getLength()));
        return ack;
    }
    public void send(){
        //Ack odesilam
        //Seq odesilam 0

    }
    public void save(){
        System.out.println("saving photo");
        confirmWindow.save();
    }


    public static byte[] constructInitialPacket(){
                         //ID                //SEQ     //ACK     //flags    //data
        byte[] initData = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,Packet.SYN,0x01};
        return initData;
    }
    public static byte[] constructFinalPacket(Byte[] id,byte[] ack){
                            //ID                //SEQ     //ACK     //flags    //data
        byte[] initData = {id[0],id[1],id[2],id[3],0x00,0x00,ack[0],ack[1],Packet.FIN};
        return initData;
    }
    public static byte[] constructErrorPacket(Byte[] id){
        //ID                //SEQ     //ACK     //flags    //data
        byte[] initData = {id[0],id[1],id[2],id[3],0x00,0x00,0x00,0x00,Packet.RST};
        return initData;
    }
}

//Packet
class Packet{
    public static final int HEADER_SIZE = ConnectionID.ID_SIZE + SequentialNumber.SEQ_SIZE + ConfirmNumber.ACK_SIZE + Symptom.INDEX_SIZE;
    public static final byte RST            = 0x01;
    public static final byte FIN            = 0x02;
    public static final byte SYN            = 0x04;

    SequentialNumber sequentialNumber;
    ConfirmNumber confirmNumber;
    Symptom symptom;
    Data data;
    int size;

    public Packet(DatagramPacket datagramPacket, ConnectionID baseConnectionID, int size) {
        if (!verifyRecievedDatagramPacketLenght(datagramPacket)){
            throw new IllegalArgumentException("Nespravna delka datagramu");
        }
        byte[] recievedData = datagramPacket.getData();
        this.size = size - HEADER_SIZE;
        this.data = new Data();


        Byte[] recievedConnectionID = new Byte[ConnectionID.ID_SIZE];
        recievedConnectionID[0] = recievedData[0];
        recievedConnectionID[1] = recievedData[1];
        recievedConnectionID[2] = recievedData[2];
        recievedConnectionID[3] = recievedData[3];
        if (!baseConnectionID.isCurrentConnectionIDSet()){
            baseConnectionID.saveRecievedID(recievedConnectionID);
        } else {
            if (!baseConnectionID.checkRecievedID(recievedConnectionID)){
                throw new IllegalArgumentException("Connection IDs se neshoduji");
            }
        }

        Byte[] recievedSequentialNumber = new Byte[SequentialNumber.SEQ_SIZE];
        recievedSequentialNumber[0] = recievedData[4];
        recievedSequentialNumber[1] = recievedData[5];
        this.sequentialNumber = new SequentialNumber(recievedSequentialNumber);

        Byte[] recievedConfirmNumber = new Byte[ConfirmNumber.ACK_SIZE];
        recievedConfirmNumber[0] = recievedData[6];
        recievedConfirmNumber[1] = recievedData[7];
        this.confirmNumber = new ConfirmNumber(recievedConfirmNumber);

        Byte recievedSymptom = recievedData[8];
        this.symptom = new Symptom(recievedSymptom);
        this.symptom.verifySymptom(recievedSymptom);


        for (int i = 9; i < recievedData.length; i++){
            if (!data.addData(recievedData[i])){
                throw new IllegalStateException("Data holder pretekl");
            }
        }
    }
    private boolean verifyRecievedDatagramPacketLenght(DatagramPacket datagramPacket){
        if (datagramPacket.getLength() < HEADER_SIZE || datagramPacket.getLength() > (HEADER_SIZE + 255)){
            return false;
        }
        return true;
    }

}
class ConnectionID{
    public static final int ID_SIZE = 4;

    private Byte[] currentConnectionID;

    public ConnectionID() {}

    public boolean isCurrentConnectionIDSet(){
        if (currentConnectionID == null){
            return false;
        } else {
            return true;
        }
    }

    public boolean checkRecievedID(Byte[] recievedConnectionID){
        if (!isCurrentConnectionIDSet()){
            throw new IllegalStateException("Id spojeni nebylo dosud nastaveno, neni co kontrolovat");
        }
        if (recievedConnectionID == null){
            return false;
        }
        if (!checkIDLenght(recievedConnectionID)){
            return false;
        }

        for (int i = 0; i < ID_SIZE; i++){
            if (currentConnectionID[i] != recievedConnectionID[i]){
                return false;
            }
        }
        return true;
    }
    public void saveRecievedID(Byte[] recievedConnectionID){
        if (recievedConnectionID == null){
            throw new IllegalArgumentException("Id ktere se snazis ulozit je null");
        }
        if (isCurrentConnectionIDSet()){
            throw new IllegalStateException("Id spojeni jiz bylo nastaveno");
        }
        System.out.println("id saved");
        this.currentConnectionID = recievedConnectionID;
        boolean he = false;
        for (int i = 0; i < currentConnectionID.length; i++) {
            if (currentConnectionID[i] != 0){
                he = true;
            }
        }
        if (!he){
            throw new IllegalArgumentException("ID is 0 0 0 0");
        }

    }
    public Byte[] getCurrentConnectionID(){
        if (!isCurrentConnectionIDSet()){
            throw new IllegalStateException("Id spojeni nebylo dosud nastaveno, neni co ziskat");
        }
        return this.currentConnectionID;
    }

    private boolean checkIDLenght(Byte[] recievedConnectionID){
        if (recievedConnectionID.length != 4){
            return false;
        }
        return true;
    }

}
class SequentialNumber implements Comparable{
    public static final int SEQ_SIZE = 2;
    public static final int MAX_SEQ  = 65535;

    private Byte[] sequentialNumber;

    public SequentialNumber(Byte[] sequentialNumber) {
        if (sequentialNumber.length != 2){
            throw new IllegalArgumentException("Bad sequential number lenght");
        }
        this.sequentialNumber = sequentialNumber;
    }

    public Byte[] getSequentialNumber() {
        return sequentialNumber;
    }
    public int getSequentialNumberDecimal() {
        int result = 0;
        int shift = 8;

        for (int i = 0; i < SEQ_SIZE; i++) {
            int part = (int)sequentialNumber[i] & 0xFF;

            result = result ^ (part << shift);
            shift -= 8;
        }
        return result;
    }
    public static int getSequentialNumberDecimal(byte[] num) {
        int result = 0;
        int shift = 8;

        for (int i = 0; i < SEQ_SIZE; i++) {
            int part = (int)num[i] & 0xFF;

            result = result ^ (part << shift);
            shift -= 8;
        }
        return result;
    }
    public static byte[] seqToBigEnd(int seqDecimal){
        byte[] res = new byte[2];
        res[0] = (byte) (seqDecimal >> 8);
        res[1] = (byte) ((seqDecimal << 8) >> 8);

        return res;
    }


    @Override
    public int compareTo(Object o) {
        Byte[] recieved = ((SequentialNumber)o).getSequentialNumber();

        if (this.getSequentialNumber()[0] < recieved[0]){
            return -1;
        } else if (this.getSequentialNumber()[0] > recieved[0]){
            return 1;
        } else {
            if (this.getSequentialNumber()[1] < recieved[1]){
                return -1;
            } else if (this.getSequentialNumber()[1] > recieved[1]){
                return 1;
            } else {
                return 0;
            }
        }
    }
}
class ConfirmNumber implements Comparable{
    public static final int ACK_SIZE = 2;
    public static final int MAX_ACK  = 65535;

    private Byte[] confirmNumber;

    public ConfirmNumber(Byte[] confirmNumber) {
        if (confirmNumber.length != 2){
            throw new IllegalArgumentException("Bad sequential number lenght");
        }
        this.confirmNumber = confirmNumber;
    }

    public Byte[] getConfirmNumber() {
        return confirmNumber;
    }

    @Override
    public int compareTo(Object o) {
        Byte[] recieved = ((ConfirmNumber)o).getConfirmNumber();

        if (this.getConfirmNumber()[0] < recieved[0]){
            return -1;
        } else if (this.getConfirmNumber()[0] > recieved[0]){
            return 1;
        } else {
            if (this.getConfirmNumber()[1] < recieved[1]){
                return -1;
            } else if (this.getConfirmNumber()[1] > recieved[1]){
                return 1;
            } else {
                return 0;
            }
        }
    }
}
class Symptom{
    public static final int INDEX_SIZE = 1;

    public static final byte RST = 0x01;
    public static final byte FIN = 0x02;
    public static final byte SYN = 0x04;

    private Byte symptom;

    public Symptom(Byte symptom) {
        this.symptom = symptom;
    }
    public boolean verifySymptom(Byte symptom){
        if (symptom != RST || symptom != FIN || symptom != SYN){
            return false;
        }
        return true;
    }



}
class Data{
    public static final int DATA_MAX = 255;

    List<Byte> data;

    public Data() {
        this.data = new ArrayList<>();
    }
    public boolean addData(Byte b){
        if (data.size() > 255){
            return false;
        }
        this.data.add(b);
        return true;
    }

}
class Photo{
    List<Byte> data;

    public Photo() {
        this.data = new ArrayList<>();
    }

    public void addPacketData(List<Byte> data){
        for (Byte b : data){
            this.data.add(b);
        }
    }
    public void savePhoto(){
        byte[] data = new byte[this.data.size()];

        for (int i = 0; i < this.data.size(); i++) {
            data[i] = this.data.get(i);
        }
        Path file = Paths.get("foto.png");
        try {
            Files.write(file, data);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}

class ConfirmWindow{
    public static final int CONFIRM_WINDOW_SIZE = 8; //2040

    private Packet[] window;
    private int start;
    private int lastConfirmedSeq;
    private Photo photo;
    static int overFlowCNT = 0;
    boolean overFlow = false;

    public ConfirmWindow(Photo photo) {
        this.window = new Packet[CONFIRM_WINDOW_SIZE];
        this.start  = 0;
        this.lastConfirmedSeq = -255;
        this.photo = photo;
    }
    public int addPacket(Packet packet){
        int shift;
        int diff = packet.sequentialNumber.getSequentialNumberDecimal() - lastConfirmedSeq;
        if (true){
            if (diff < -45000 & !overFlow){
                System.out.println("got overflowed");
                overFlow = true;
                overFlowCNT++;

            } else if (diff > 0 & overFlow){
                System.out.println("got underflowed");
                overFlow = false;
                overFlowCNT--;
            }
            else if (diff > 3000 & diff < 0){
                System.out.println("\033[43;1m"+"Some shit"+"\033[0;0m");
            }
        }
        diff = Math.floorMod(diff,65535);
        diff += overFlowCNT;


        if ((diff < 255) && (diff > 0)) {
            shift = start + 1;
        } else {
            if (((diff % 255) * 255) == 0){
                shift = (start + (diff / 255)-1);
            } else {
               //System.out.println("ENDE: "+packet.data.data.size());
                System.out.println(packet.sequentialNumber.getSequentialNumberDecimal() +" - "+ lastConfirmedSeq+ " " + diff);
                shift = (start + ((diff+1) / 255));
                return lastConfirmedSeq - packet.sequentialNumber.getSequentialNumberDecimal();
            }
        }

        //int dest =  shift % 8;
        int dest = Math.floorMod(shift,8);
        if (dest < 0){
            System.out.println("dest je zaporne");
            return lastConfirmedSeq;
        }

        if (packet.sequentialNumber.getSequentialNumberDecimal() > maxSeqAllowed()){
            System.out.println("prisel moc vysoky packet" + packet.sequentialNumber.getSequentialNumberDecimal() );
            return lastConfirmedSeq;
        }

        this.window[dest] = packet;


        System.out.print("DUMP: ");
        for (int i = 0; i < 8; i++) {
            if (i == start){
                if(this.window[i] != null){System.out.print("\033[32;1m"+"["+i+"]"+this.window[i].sequentialNumber.getSequentialNumberDecimal()+"\033[0;0m"+" ");}else{System.out.print("\033[32;1m"+"["+i+"]"+"null"+"\033[0;0m");}
            } else {
                if(this.window[i] != null){System.out.print("["+i+"]"+this.window[i].sequentialNumber.getSequentialNumberDecimal()+" ");}else{System.out.print("["+i+"]"+"null");}
            }
        }
        ///System.out.println(" ");

        this.start = this.start % 8;
        return proceedConfirmation();
    }
    private int proceedConfirmation(){
        int point = start;

        for (int i = 0; i < 8; i++) {
            if (window[point] != null){
                int difference = window[point].sequentialNumber.getSequentialNumberDecimal() - (lastConfirmedSeq);
                difference = Math.floorMod(difference,65535);
                difference += overFlowCNT;

                if ((difference <= 255) && (difference > 0)){
                    photo.addPacketData(window[point].data.data);
                    if (window[point].size < 255){
                        System.out.print("LAST SIZE: "+ window[point].size+" on "+window[point].sequentialNumber.getSequentialNumberDecimal());
                        return window[point].sequentialNumber.getSequentialNumberDecimal() - (255-window[point].size);
                    }
                    lastConfirmedSeq = window[point].sequentialNumber.getSequentialNumberDecimal();


                    window[point] = null;
                    start++;
                    this.start = this.start % 8;
                }
                else {
                    return lastConfirmedSeq;
                }
                point++;
                point = point % 8;
            }
        }
        return lastConfirmedSeq;
    }
    public void save(){
        this.photo.savePhoto();
    }


    public Packet[] getWindow(){
        return this.window;
    }

    private int maxSeqAllowed(){
        int max = (lastConfirmedSeq + (8 * 255)) % (SequentialNumber.MAX_SEQ+1);
        if (max < lastConfirmedSeq){
            return 65535;
        }
        return max;
    }




}
class Timeout{
    public static final int TIMEOUT = 100; //in miliseconds

}

class DeInit{}

/*
class Test{
    public Test() {
        ConnectionID baseCID = new ConnectionID();
        Byte[] bb = {0x01,0x20,0x03,0x40};
        if (!baseCID.isCurrentConnectionIDSet()){
            baseCID.saveRecievedID(bb);
        }



        //ID                 //SEQ       //ACK     //flags    //data
        //  byte[] initData = {0x00,0x00,0x00,0x00, 0x00,0x00,  0x00,0x00,  0x00,  0x01};

        byte[] one      = {0x01,0x20,0x03,0x40, 0x00,0x00,  0x00,0x00,  0x00,  0x01}; //seq=0

        byte[] two      = {0x01,0x20,0x03,0x40, 0x00,(byte)0xFF,  0x00,0x00,  0x00,  0x02}; //seq=255

        byte[] thr      = {0x01,0x20,0x03,0x40, 0x01,(byte)0xFE,  0x00,0x00,  0x00,  0x03}; //seq=510

        byte[] fou      = {0x01,0x20,0x03,0x40, 0x02,(byte)0xFD,  0x00,0x00,  0x00,  0x04}; //seq=765

        byte[] fiv      = {0x01,0x20,0x03,0x40, 0x03,(byte)0xFC,  0x00,0x00,  0x00,  0x05}; //seq=1020

        byte[] six      = {0x01,0x20,0x03,0x40, 0x04,(byte)0xFB,  0x00,0x00,  0x00,  0x06}; //seq=1275

        byte[] sev      = {0x01,0x20,0x03,0x40, 0x05,(byte)0xFA,  0x00,0x00,  0x00,  0x07}; //seq=1530

        byte[] eig      = {0x01,0x20,0x03,0x40, 0x06,(byte)0xF9,  0x00,0x00,  0x00,  0x08}; //seq=1785

        byte[] nin      = {0x01,0x20,0x03,0x40, 0x07,(byte)0xF8,  0x00,0x00,  0x00,  0x09}; //seq=2040

        byte[] ten      = {0x01,0x20,0x03,0x40, 0x08,(byte)0xF7,  0x00,0x00,  0x00,  0x0a}; //seq=2295

        byte[] ele      = {0x01,0x20,0x03,0x40, 0x09,(byte)0xF6,  0x00,0x00,  0x00,  0x0b}; //seq=2550

        byte[] twe      = {0x01,0x20,0x03,0x40, 0x0A,(byte)0xF5,  0x00,0x00,  0x00,  0x0c}; //seq=2805

        byte[] las      = {0x01,0x20,0x03,0x40, 0x0A,(byte)0xF8,  0x00,0x00,  0x00}; //seq=2808


        Packet oneP = new Packet(new DatagramPacket(one,one.length),baseCID);
        Packet twoP = new Packet(new DatagramPacket(two,two.length),baseCID);
        Packet thrP = new Packet(new DatagramPacket(thr,thr.length),baseCID);
        Packet fouP = new Packet(new DatagramPacket(fou,fou.length),baseCID);
        Packet fivP = new Packet(new DatagramPacket(fiv,fiv.length),baseCID);
        Packet sixP = new Packet(new DatagramPacket(six,six.length),baseCID);
        Packet sevP = new Packet(new DatagramPacket(sev,sev.length),baseCID);
        Packet eigP = new Packet(new DatagramPacket(eig,eig.length),baseCID);
        Packet ninP = new Packet(new DatagramPacket(nin,nin.length),baseCID);
        Packet tenP = new Packet(new DatagramPacket(ten,ten.length),baseCID);
        Packet eleP = new Packet(new DatagramPacket(ele,ele.length),baseCID);
        Packet tweP = new Packet(new DatagramPacket(twe,twe.length),baseCID);

        Packet lasP = new Packet(new DatagramPacket(las,las.length),baseCID);






        Photo p = new Photo();
        ConfirmWindow c = new ConfirmWindow(p);
//        Classic
        c.addPacket(oneP);
        c.addPacket(twoP);
        c.addPacket(thrP);
        c.addPacket(fouP);

        c.addPacket(fivP);
        c.addPacket(sixP);
        c.addPacket(sevP);
        c.addPacket(eigP);

        c.addPacket(ninP);

        c.addPacket(lasP);

        c.addPacket(tenP);
        c.addPacket(eleP);
        c.addPacket(tweP);



////        Random
        c.addPacket(twoP);
        c.addPacket(thrP);
        c.addPacket(fouP);
        c.addPacket(tweP);
        c.addPacket(oneP);
        c.addPacket(fivP);
        c.addPacket(sixP);
        c.addPacket(sevP);
        c.addPacket(fivP);
        c.addPacket(fivP);
        c.addPacket(fivP);
        c.addPacket(fivP);
        c.addPacket(sixP);
        c.addPacket(sevP);
        c.addPacket(twoP);
        c.addPacket(thrP);
        c.addPacket(fouP);

        c.addPacket(fivP);
        c.addPacket(sixP);
        c.addPacket(sevP);
        c.addPacket(eigP);

        c.addPacket(ninP);
        c.addPacket(tenP);
        c.addPacket(eleP);
        c.addPacket(tweP);
        c.addPacket(lasP);


        p.savePhoto();
        System.out.println("ENDE");
    }
*/